import { System } from '@serpentity/serpentity';

import HueShiftableNode from '../nodes/hue_shiftable';

/**
 * Shifts the hue of a hue shiftable node
 *
 * @extends {external:Serpentity.System}
 * @param {object} config a configuration object to extend.
 */
export default class ShiftHueSystem extends System {

  constructor(config = {}) {

    super();

    /**
     * The node collection of hue shiftable entities
     *
     * @type external:Serpentity.NodeCollection
     */
    this.hueShiftables = null;
  }

  /**
   * Initializes system when added. Requests hue shiftable nodes
   *
   * @function added
   * @param {external:Serpentity.Engine} engine the serpentity engine to
   * which we are getting added
   */
  added(engine) {

    this.hueShiftables = engine.getNodes(HueShiftableNode);
  }

  /**
   * Clears system resources when removed.
   *
   * @function removed
   */
  removed() {

    this.hueShiftables = null;
  }

  /**
   * Runs on every update of the loop. Shifts hue randomly because
   * I didn't read up on the color matrix in time to do it slowly
   *
   * @function update
   * @param {Number} currentFrameDuration the duration of the current
   * frame
   */
  update(currentFrameDuration) {

    for (const hueShiftable of this.hueShiftables) {
      hueShiftable.filter.filter.hue(Math.random() * 10 * currentFrameDuration % 360);
    }
  }
};
