import { System } from '@serpentity/serpentity';

import RotatableNode from '../nodes/rotatable';

const internals = {
  kRotationalVelocity: 0.001
};

/**
 * Rotates rotatable items on each frame
 *
 * @extends {external:Serpentity.System}
 * @param {object} config a configuration object to extend.
 */
export default class RotateSystem extends System {

  constructor(config = {}) {

    super();

    /**
     * The node collection of rotatable entities
     *
     * @type external:Serpentity.NodeCollection
     */
    this.rotatables = null;
  }

  /**
   * Initializes system when added. Requests rotatable nodes
   *
   * @function added
   * @param {external:Serpentity.Engine} engine the serpentity engine to
   * which we are getting added
   */
  added(engine) {

    this.rotatables = engine.getNodes(RotatableNode);
  }

  /**
   * Clears system resources when removed.
   *
   * @function removed
   */
  removed() {

    this.rotatables = null;
  }

  /**
   * Runs on every update of the loop. Rotates slightly
   *
   * @function update
   * @param {Number} currentFrameDuration the duration of the current
   * frame
   */
  update(currentFrameDuration) {

    for (const rotatable of this.rotatables) {
      rotatable.angle.angle += currentFrameDuration * internals.kRotationalVelocity;
      rotatable.angle.angle = rotatable.angle.angle % (2 * Math.PI);
    }
  }
};
