import { System } from '@serpentity/serpentity';

import TemporaryNode from '../nodes/temporary';

/**
 * Ages temporary nodes
 *
 * @extends {external:Serpentity.System}
 * @param {object} config a configuration object to extend.
 */
export default class AgeEntitiesSystem extends System {

  constructor(config = {}) {

    super();

    /**
     * The node collection of temporary entities
     *
     * @type external:Serpentity.NodeCollection
     */
    this.temporaries = null;
  }

  /**
   * Initializes system when added. Requests temporary entities
   *
   * @function added
   * @param {external:Serpentity.Engine} engine the serpentity engine to
   * which we are getting added
   */
  added(engine) {

    this.temporaries = engine.getNodes(TemporaryNode);
  }

  /**
   * Clears system resources when removed.
   *
   * @function removed
   */
  removed() {

    this.temporaries = null;
  }

  /**
   * Runs on every update of the loop. Adds current frame duration
   * to lifespan
   *
   * @function update
   * @param {Number} currentFrameDuration the duration of the current
   * frame
   */
  update(currentFrameDuration) {

    for (const temporary of this.temporaries) {
      temporary.lifespan.age += currentFrameDuration;
    }
  }
};
