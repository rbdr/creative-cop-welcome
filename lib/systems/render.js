import { System } from '@serpentity/serpentity';

import RenderableNode from '../nodes/renderable';

const internals = {
  kNoPixiError: 'No pixi application passed to render system. Make sure you set the `application` key in the config object when initializing.'
};

/**
 * Renders renderable objects using pixi
 *
 * @extends {external:Serpentity.System}
 * @param {object} config a configuration object to extend.
 */
export default class RenderSystem extends System {

  constructor(config = {}) {

    super();

    /**
     * The node collection of renderable entities
     *
     * @type external:Serpentity.NodeCollection
     */
    this.renderables = null;

    /**
     * The pixi engine we will use to render
     *
     * @type external:PixiJs.Application
     */
    this.application = config.application;

    if (!this.application) {
      throw new Error(internals.kNoPixiError);
    }
  }

  /**
   * Initializes system when added. Requests renderable nodes and
   * attaches to event listeners to add / remove them to pixi stage
   *
   * @param {external:Serpentity.Engine} engine the serpentity engine to
   * which we are getting added
   */
  added(engine) {

    this.renderables = engine.getNodes(RenderableNode);
    this.renderables.on('nodeAdded', (event) => {

      this.application.stage.addChild(event.node.container.container);
    });
    this.renderables.on('nodeRemoved', (event) => {

      this.application.stage.removeChild(event.node.container.container);
    });
  }

  /**
   * Clears system resources when removed.
   */
  removed() {

    this.renderables.removeAllListeners('nodeAdded');
    this.renderables.removeAllListeners('nodeRemoved');
    this.renderables = null;
  }

  /**
   * Runs on every update of the loop. Renders.
   */
  update() {

    this.application.render();
  }
};
