import { System } from '@serpentity/serpentity';

import TemporaryNode from '../nodes/temporary';

/**
 * Removes old entities
 *
 * @extends {external:Serpentity.System}
 * @param {object} config a configuration object to extend.
 */
export default class RemoveOldEntitiesSystem extends System {

  constructor(config = {}) {

    super();

    /**
     * The node collection of temporary entities
     *
     * @type external:Serpentity.NodeCollection
     */
    this.temporaries = null;
  }

  /**
   * Initializes system when added. Requests temporary entities
   *
   * @function added
   * @param {external:Serpentity.Engine} engine the serpentity engine to
   * which we are getting added
   */
  added(engine) {

    this._engine = engine;
    this.temporaries = engine.getNodes(TemporaryNode);
  }

  /**
   * Clears system resources when removed.
   *
   * @function removed
   */
  removed() {

    this.temporaries = null;
    this._engine = null;
  }

  /**
   * Runs on every update of the loop. Removes entities when they are
   * too old
   *
   * @function update
   * @param {Number} currentFrameDuration the duration of the current
   * frame
   */
  update(currentFrameDuration) {

    for (const temporary of this.temporaries) {
      if (temporary.lifespan.age > temporary.lifespan.ttl) {
        this._engine.removeEntity(temporary.entity);
      }
    }
  }
};
