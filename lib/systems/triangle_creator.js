import { System } from '@serpentity/serpentity';

import Config from '../config';
import TriangleFactory from '../factories/triangle';

const internals = {
  kJitter: 50,
  kPeriod: 144
};

/**
 * Adds a triangle at a fixed period with a certain amount of jitter
 *
 * @extends {external:Serpentity.System}
 * @param {object} config a configuration object to extend.
 */
export default class TriangleCreatorSystem extends System {

  constructor(config = {}) {

    super();

    /**
     * The current period
     *
     * @type number
     */
    this.currentPeriod = 0;
  }

  /**
   * Initializes system when added. Resets current period
   *
   * @param {external:Serpentity.Engine} engine the serpentity engine to
   * which we are getting added
   */
  added(engine) {

    this.currentPeriod = 0;

    this._engine = engine;
  }

  /**
   * Clears system resources when removed.
   */
  removed() {

    this._engine = null;
  }

  /**
   * runs on every update of the loop. Updates current period and
   * creates a triangle if > the set period.
   *
   * @function update
   * @param {number} currentFrameDuration the duration of the current
   * frame
   */
  update(currentFrameDuration) {

    this.currentPeriod += currentFrameDuration;

    if (this.currentPeriod >= internals.kPeriod) {
      TriangleFactory.createTriangle(this._engine, {
        position: {
          x: Config.horizontalResolution / 2 + (Math.random() * internals.kJitter - internals.kJitter / 2),
          y: Config.verticalResolution / 2 + (Math.random() * internals.kJitter - internals.kJitter / 2)
        }
      });

      this.currentPeriod = this.currentPeriod % internals.kPeriod;
    }
  }
};
