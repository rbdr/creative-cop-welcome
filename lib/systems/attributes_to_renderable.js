import { System } from '@serpentity/serpentity';

import RenderableWithAttributesNode from '../nodes/renderable_with_attributes';

/**
 * Updates the renderables based on their attribuets
 *
 * @extends {external:Serpentity.System}
 * @param {object} config a configuration object to extend.
 */
export default class AttributesToRenderableSystem extends System {

  constructor(config = {}) {

    super();

    /**
     * The node collection of renderable entities
     *
     * @type external:Serpentity.NodeCollection
     */
    this.renderables = null;
  }

  /**
   * Initializes system when added. Requests renderable nodes and
   * attaches to event listeners to add / remove them to pixi stage
   *
   * @param {external:Serpentity.Engine} engine the serpentity engine to
   * which we are getting added
   */
  added(engine) {

    this.renderables = engine.getNodes(RenderableWithAttributesNode);
  }

  /**
   * Clears system resources when removed.
   */
  removed() {

    this.renderables = null;
  }

  /**
   * Runs on every update of the loop. Updates the graphics so they're
   * rendered correctly
   */
  update() {

    for (const renderable of this.renderables) {
      renderable.container.container.rotation = renderable.angle.angle;
    }
  }
};
