/**
 * Holds the main configuration of the application
 * @name Config
 * @type object
 */
const config = {

  /**
   * Aspect Ratio the aspect ratio expressed as an array of two numbers
   *
   * @property {number} aspectRatio
   * @memberof Config
   */
  aspectRatio: [2.76, 1],

  /**
   * Target vertical resolution
   *
   * @property {number} verticalResolution
   * @memberof Config
   */
  verticalResolution: 224
};

/**
 * The horizontal resolution of the screen
 *
 * @property {number} horizontalResolution
 * @memberof Config
 */
config.horizontalResolution = Math.round(config.verticalResolution * config.aspectRatio[0] / config.aspectRatio[1]);

export default config;
