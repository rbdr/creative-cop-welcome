import { Component } from '@serpentity/serpentity';

/**
 * Component that stores an angle
 *
 * @extends {external:Serpentity.Component}
 * @class AngleComponent
 * @param {object} config a configuration object to extend.
 */
export default class AngleComponent extends Component {
  constructor(config) {

    super(config);

    /**
     * The properthy that holds the angle
     *
     * @property {number} angle
     * @instance
     * @memberof AngleComponent
     */
    this.angle = this.angle || 0;
  }
};

