import { Component } from '@serpentity/serpentity';

const internals = {
  kDefaultTTL: 4000
};

/**
 * Component that stores a Time to Live
 *
 * @extends {external:Serpentity.Component}
 * @param {object} config a configuration object to extend.
 */
export default class TTLComponent extends Component {
  constructor(config) {

    super(config);

    /**
     * The time to live
     *
     * @type number
     */
    this.ttl = this.ttl || internals.kDefaultTTL;

    /**
     * The time lived
     *
     * @type number
     */
    this.age = 0;
  }
};
