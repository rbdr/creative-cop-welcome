import { Component } from '@serpentity/serpentity';

/**
 * Component that stores a Pixi Filter
 *
 * @extends {external:Serpentity.Component}
 * @param {object} config a configuration object to extend.
 */
export default class ColorComponent extends Component {
  constructor(config) {

    super(config);

    /**
     * The properthy that holds the angle
     *
     * @type external:PixiJS.ColorMatrixFilter
     */
    this.filter = this.filter;
  }
};

