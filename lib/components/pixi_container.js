import { Component } from '@serpentity/serpentity';

/**
 * Component that stores a pixi container
 *
 * @extends {external:Serpentity.Component}
 * @class PixiContainerComponent
 * @param {object} config a configuration object to extend.
 */
export default class PixiContainerComponent extends Component {
  constructor(config) {

    super(config);

    /**
     * The properthy that holds the pixi container
     *
     * @property {external:PixiJs.Container} container
     * @name container
     * @instance
     * @memberof PixiContainerComponent
     */
    this.container = this.container || null;
  }
};
