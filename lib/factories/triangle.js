// External Dependencies

import { Entity } from '@serpentity/serpentity';

// Components

import AngleComponent from '../components/angle';
import PixiContainerComponent from '../components/pixi_container';
import PixiFilterComponent from '../components/pixi_filter';
import PositionComponent from '@serpentity/components.position';
import TTLComponent from '../components/ttl';

// Factories

import PixiFactory from '../factories/pixi';

/**
 * Factory object that creates new triangles
 *
 * @type object
 * @name TriangleFactory
 */
export default {

  /**
   * Creates a triangle entity and adds it to the engine. Can override
   * position in the config object
   *
   * @param {external:Serpentity} [engine] the serpentity engine to attach
   * to. If not sent, it will not be attached.
   * @param {object} [config] the config to override the entity, accepts
   * the key `position` as an object with an x and y property.
   * @return {external:Serpentity.Entity} the created entity
   */
  createTriangle(engine, config = {}) {

    const entity = new Entity();

    // POSITION

    entity.addComponent(new PositionComponent(config.position));
    const position = entity.getComponent(PositionComponent);

    entity.addComponent(new AngleComponent(config.angle));
    const angle = entity.getComponent(AngleComponent);

    entity.addComponent(new TTLComponent());

    // RENDERING

    const colorFilter = PixiFactory.createColorFilter();
    entity.addComponent(new PixiFilterComponent({
      filter: colorFilter
    }));

    const radius = 100;
    const pixiConfig = Object.assign({
      colorFilter,
      radius
    }, config.pixi);

    const container = config.container || {
      container: PixiFactory.createTriangle(pixiConfig)
    };
    container.container.position.x = position.x;
    container.container.position.y = position.y;
    container.container.angle = angle.angle;
    entity.addComponent(new PixiContainerComponent(container));

    if (engine) {
      engine.addEntity(entity);
    }

    return entity;
  }
};
