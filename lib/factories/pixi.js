import { Graphics, filters } from 'pixi.js';

/**
 * Factory object that contains methods to create prefab pixi
 * objects
 *
 * @type object
 * @name PixiFactory
 */
export default {

  /**
   * Creates a triangle container
   *
   * @function createTriangle
   * @return {external:PixiJS.Container} the created container
   */
  createTriangle(config) {

    const radius = config.radius;
    const triangleColor = config.color || 0x87c5ea;

    // The body
    const triangle = new Graphics();
    triangle.lineStyle(1, triangleColor)
      // Top vertex
      .moveTo(radius * Math.cos(270 * Math.PI / 180),
        radius * Math.sin(270 * Math.PI / 180))

      // Right Vertex
      .lineTo(radius * Math.cos(30 * Math.PI / 180),
        radius * Math.sin(30 * Math.PI / 180))

      // Left Vertex
      .lineTo(radius * Math.cos(150 * Math.PI / 180),
        radius * Math.sin(150 * Math.PI / 180))

      // Close Line
      .lineTo(radius * Math.cos(270 * Math.PI / 180),
        radius * Math.sin(270 * Math.PI / 180));

    if (config.colorFilter) {
      triangle.filters = [config.colorFilter];
    }

    return triangle;
  },

  /**
   * Creates a color matrix filter
   *
   * @function createColorFilter
   * @return {external:PixiJS.ColorMatrixFilter} the created container
   */
  createColorFilter() {

    const colorFilter = new filters.ColorMatrixFilter();

    return colorFilter;
  }
};
