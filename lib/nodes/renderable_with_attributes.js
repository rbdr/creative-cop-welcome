import { Node } from '@serpentity/serpentity';

import AngleComponent from '../components/angle';
import PixiContainerComponent from '../components/pixi_container';

/**
 * Node identifying a renderable entity with an angle
 *
 * @extends {external:Serpentity.Node}
 */
export default class RenderableWithAttributesNode extends Node {

};

/**
 * Holds the types that are used to identify a renderable with external
 * attributes
 *
 * @type object
 */
RenderableWithAttributesNode.types = {
  angle: AngleComponent,
  container: PixiContainerComponent
};
