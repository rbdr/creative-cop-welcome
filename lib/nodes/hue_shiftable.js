import { Node } from '@serpentity/serpentity';

import PixiFilterComponent from '../components/pixi_filter';

/**
 * Node identifying a hue shiftable entity, should have a pixi filter
 *
 * @extends {external:Serpentity.Node}
 */
export default class HueShiftableNode extends Node {

};

/**
 * Holds the types that are used to identify a hue shiftable entity
 *
 * @type object
 */
HueShiftableNode.types = {
  filter: PixiFilterComponent
};
