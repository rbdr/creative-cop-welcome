import { Node } from '@serpentity/serpentity';

import TTLComponent from '../components/ttl';

/**
 * Node identifying a hue shiftable entity, should have a pixi filter
 *
 * @extends {external:Serpentity.Node}
 */
export default class TemporaryNode extends Node {

};

/**
 * Holds the types that are used to identify a temporary entity
 *
 * @type object
 */
TemporaryNode.types = {
  lifespan: TTLComponent
};
