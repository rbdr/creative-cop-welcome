import { Node } from '@serpentity/serpentity';

import AngleComponent from '../components/angle';

/**
 * Node identifying a rotatable entity
 *
 * @extends {external:Serpentity.Node}
 */
export default class RotatableNode extends Node {

};

/**
 * Holds the types that are used to identify a rotatable
 *
 * @type object
 */
RotatableNode.types = {
  angle: AngleComponent
};
