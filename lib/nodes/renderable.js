import { Node } from '@serpentity/serpentity';

import PixiContainerComponent from '../components/pixi_container';

/**
 * Node identifying a renderable entity, should have a pixi renderable
 *
 * @extends {external:Serpentity.Node}
 */
export default class RenderableNode extends Node {

};

/**
 * Holds the types that are used to identify a renderable entity
 *
 * @type object
 */
RenderableNode.types = {
  container: PixiContainerComponent
};
