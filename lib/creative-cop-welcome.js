import 'idempotent-babel-polyfill';

import Config from './config';

// Systems

import AgeEntitiesSystem from './systems/age_entities';
import AttributesToRenderableSystem from './systems/attributes_to_renderable';
import ShiftHueSystem from './systems/shift_hue';
import RemoveOldEntitiesSystem from './systems/remove_old_entities';
import RenderSystem from './systems/render';
import RotateSystem from './systems/rotate';
import TriangleCreatorSystem from './systems/triangle_creator';

// Factories

import TriangleFactory from './factories/triangle';

// External Dependencies

import Serpentity from '@serpentity/serpentity';
import { Application } from 'pixi.js';

/* global window document */

const internals = {
  kBackgroundColor: 0x000000,
  kNoElementError: 'No element found. Cannot render.',

  // Handler for the window load event. Initializes and runs the app.

  onLoad() {

    const creativeCopWelcome = new internals.CreativeCopWelcome(Object.assign({
      element: document.getElementById('creative-cop-welcome-entry-point')
    }, Config));

    creativeCopWelcome.startLoop();

    // Attaching to window to make debugging easier

    window.creativeCopWelcome = creativeCopWelcome;
  }
};

/**
 * Main entry point. Attached to window->load
 *
 * @param {object} config the configuration to extend the object
 *
 * @property {HTMLElement} [element=null] the element in which to render.
 * Required, will throw if not provided
 * @property {Number} [fps=60] the fps target to maintain
 * @property {Number} [verticalResolution=224] how many pixels to render in the vertical
 * axis (gets scaled if the canvas is larger)
 * @property {Array<Number>} [aspectRatio=[2.76, 1]] the aspect ratio experssed as
 * an array of two numbers, where aspect ratio x:y is [x, y] (eg. [16, 9])
 */

internals.CreativeCopWelcome = class CreativeCopWelcome {

  constructor(config) {

    // These defaults can get overridden by config

    this.fps = 60;
    this.aspectRatio = [2.76, 1];
    this.verticalResolution = 224;

    Object.assign(this, config);

    if (!this.element) {
      throw new Error(internals.kNoElementError);
    }

    this._engine = new Serpentity();

    this._previousTime = 0;
    this._looping = false;

    // Initialization functions
    this._initializeCanvas();
    this._initializePixi();
    this._initializeSystems();
    this._initializeEntities();
  }

  /**
   * Starts the main loop. Resets the FPS (if you change it it won't go
   * live until after you stop and start the loop)
   */
  startLoop() {

    this._looping = true;
    this._frameDuration = 1000 / this.fps;
    window.requestAnimationFrame(this._loop.bind(this));
  }

  /**
   * Pauses the loop
   */
  pauseLoop() {

    this._looping = false;
  }

  // The main loop used above. Runs the serpentity update process and
  // attempts to maintain FPS. The rest is handled by the engine.

  _loop(currentTime) {

    if (!this._looping) {
      return;
    }

    window.requestAnimationFrame(this._loop.bind(this));

    const currentFrameDuration = currentTime - this._previousTime;

    if (currentFrameDuration > this._frameDuration) {

      this._engine.update(currentFrameDuration);
      this._previousTime = currentTime;
    }
  }

  // Creates a canvas for rendering, also attaches resize events

  _initializeCanvas() {

    this._canvas = document.createElement('canvas');
    this.element.appendChild(this._canvas);
    this._resizeCanvas();
    window.addEventListener('resize', this._resizeCanvas.bind(this));
  }

  // Initialize Pixi, it's our main rendering tool

  _initializePixi() {

    this._pixi = new Application({
      backgroundColor: internals.kBackgroundColor,
      view: this._canvas,
      width: this._canvas.width,
      height: this._canvas.height
    });
  }

  // Resizes the canvas to a square the size of the smallest magnitude
  // of the window

  _resizeCanvas() {

    let width = window.innerWidth;
    let height = Math.round(width * this.aspectRatio[1] / this.aspectRatio[0]);

    if (window.innerHeight < height) {
      height = window.innerHeight;
      width = Math.round(height * this.aspectRatio[0] / this.aspectRatio[1]);
    }

    this._canvas.style.width = `${width}px`;
    this._canvas.style.height = `${height}px`;

    this._canvas.width = Math.round(this.verticalResolution * this.aspectRatio[0] / this.aspectRatio[1]);
    this._canvas.height = this.verticalResolution;
  }

  // Initializes the serpentity systems

  _initializeSystems() {

    this._engine.addSystem(new RotateSystem());

    this._engine.addSystem(new ShiftHueSystem());

    this._engine.addSystem(new AttributesToRenderableSystem());

    this._engine.addSystem(new TriangleCreatorSystem());

    this._engine.addSystem(new AgeEntitiesSystem());

    this._engine.addSystem(new RemoveOldEntitiesSystem());

    this._engine.addSystem(new RenderSystem({
      application: this._pixi
    }));
  }

  // Initializes the serpentity entities

  _initializeEntities() {

    TriangleFactory.createTriangle(this._engine, {
      position: {
        x: this.horizontalResolution / 2,
        y: this.verticalResolution / 2
      }
    });
  }
};

export default internals.exports = {};

// autorun.bat
window.addEventListener('load', internals.onLoad);
