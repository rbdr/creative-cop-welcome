# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.1]
- Reduce rotational speed so it doesn't look jumpy

## 1.0.0
### Added
- Draws jittery triangles
- Eslint config
- JSdoc config
- Serpentity as a dependency
- Base HTML and CSS
- Wrapper application
- This CHANGELOG
- A README
- A CONTRIBUTING guide
- Pixi powered renderer system

[1.0.1]: https://gitlab.com/rbdr/creative-cop-welcome/compare/1.0.0...1.0.1
[Unreleased]: https://gitlab.com/rbdr/creative-cop-welcome/compare/master...develop
