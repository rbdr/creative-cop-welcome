# Contributing to creative-cop-welcome

This is a quick one-off project made to display in the background of
the first session of the creaative coding community of practice at
babbel, there are no intentions of continuing any further than that.
However if you think it can go further and would like to recommend
any improvements, any input is welcome

## The objective of creative-cop-welcome

A fun background image to display during the first session of the
creative coding CoP

## How to contribute

Above All: Be nice, always.

* Ensure the linter shows no warnings or errors
* Don't break the CI
* Make the PRs according to [Git Flow][gitflow]: (features go to
  develop, hotfixes go to master)

[gitflow]: https://github.com/nvie/gitflow
